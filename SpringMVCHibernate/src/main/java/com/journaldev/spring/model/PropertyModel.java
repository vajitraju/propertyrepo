package com.journaldev.spring.model;

public class PropertyModel {

	private RoomDetailsModel roomDetails;
	private AddressModel address;
	
	public RoomDetailsModel getRoomDetails() {
		return roomDetails;
	}
	public void setRoomDetails(RoomDetailsModel roomDetails) {
		this.roomDetails = roomDetails;
	}
	public AddressModel getAddress() {
		return address;
	}
	public void setAddress(AddressModel address) {
		this.address = address;
	}
}
