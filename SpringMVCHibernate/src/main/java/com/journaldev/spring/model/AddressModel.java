package com.journaldev.spring.model;

public class AddressModel {
	
	private String Address_Id;
	private String Address;
	private String Locality;
	private String Landmark;
	private String City;
	private String Postal_Code;
	private String State;
	private String Location_Id;
	
	public String getAddress_Id() {
		return Address_Id;
	}
	public void setAddress_Id(String address_Id) {
		Address_Id = address_Id;
	}
	
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getLocality() {
		return Locality;
	}
	public void setLocality(String locality) {
		Locality = locality;
	}
	public String getLandmark() {
		return Landmark;
	}
	public void setLandmark(String landmark) {
		Landmark = landmark;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getPostal_Code() {
		return Postal_Code;
	}
	public void setPostal_Code(String postal_Code) {
		Postal_Code = postal_Code;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getLocation_Id() {
		return Location_Id;
	}
	public void setLocation_Id(String location_Id) {
		Location_Id = location_Id;
	}
}
