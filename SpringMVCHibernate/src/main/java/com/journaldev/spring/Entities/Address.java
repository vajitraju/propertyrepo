package com.journaldev.spring.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="address")
public class Address {

	@Id
	@Column(name="Address_Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String Address_Id;
	
	@Column(name="Address")
	private String Address;
	
	@Column(name="Locality")
	private String Locality;
	
	@Column(name="Landmark")
	private String Landmark;
	
	@Column(name="City")
	private String City;
	
	@Column(name="Postal_Code")
	private String Postal_Code;
	
	@Column(name="State")
	private String State;
	
	@Column(name="Location_Id")
	private String Location_Id;
	
	public String getAddress_Id() {
		return Address_Id;
	}
	public void setAddress_Id(String address_Id) {
		Address_Id = address_Id;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getLocality() {
		return Locality;
	}
	public void setLocality(String locality) {
		Locality = locality;
	}
	public String getLandmark() {
		return Landmark;
	}
	public void setLandmark(String landmark) {
		Landmark = landmark;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getPostal_Code() {
		return Postal_Code;
	}
	public void setPostal_Code(String postal_Code) {
		Postal_Code = postal_Code;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getLocation_Id() {
		return Location_Id;
	}
	public void setLocation_Id(String location_Id) {
		Location_Id = location_Id;
	}
}
