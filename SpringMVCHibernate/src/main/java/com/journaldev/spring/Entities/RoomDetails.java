package com.journaldev.spring.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="property")
public class RoomDetails {

	@Id
	@Column(name="Property_Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String Property_Id;
	
	@Column(name="Cust_Id")
	private String Cust_Id;
	
	@Column(name="Property_Type")
	private String Property_Type;
	
	@Column(name="Availability_Type")
	private String Availability_Type;
	
	@Column(name="Accom_Type")
	private String Accom_Type;
	
	@Column(name="Food_Desc")
	private String Food_Desc;
	
	@Column(name="Bathroom_Type")
	private String Bathroom_Type;
	
	@Column(name="Power_Backup")
	private String Power_Backup;
	
	@Column(name="Occupancy_Type")
	private String Occupancy_Type;
	
	@Column(name="Rent_Amount")
	private String Rent_Amount;
	
	@Column(name="Wardrobe_Type")
	private String Wardrobe_Type;
	
	@Column(name="Table")
	private String Table;
	
	@Column(name="Chair")
	private String Chair;
	
	@Column(name="Geyser")
	private String Geyser;
	
	@Column(name="Total_Floor")
	private String Total_Floor;
	
	@Column(name="RoomOn_Floor")
	private String RoomOn_Floor;
	
	@Column(name="Fan")
	private String Fan;
	
	@Column(name="Bed")
	private String Bed;
	
	@Column(name="CCTV")
	private String CCTV;
	
	@Column(name="Wardeb")
	private String Wardeb;
	
	@Column(name="RO_Filter")
	private String RO_Filter;
	
	@Column(name="Immed_Avail")
	private String Immed_Avail;
	
	@Column(name="Future_Avail_Dt")
	private String Future_Avail_Dt;
	
	@Column(name="Prop_Desc")
	private String Prop_Desc;
	
	public String getProperty_Id() {
		return Property_Id;
	}
	public void setProperty_Id(String property_Id) {
		Property_Id = property_Id;
	}
	
	public String getCust_Id() {
		return Cust_Id;
	}
	public void setCust_Id(String cust_Id) {
		Cust_Id = cust_Id;
	}
	public String getProperty_Type() {
		return Property_Type;
	}
	public void setProperty_Type(String property_Type) {
		Property_Type = property_Type;
	}
	public String getAvailability_Type() {
		return Availability_Type;
	}
	public void setAvailability_Type(String availability_Type) {
		Availability_Type = availability_Type;
	}
	public String getAccom_Type() {
		return Accom_Type;
	}
	public void setAccom_Type(String accom_Type) {
		Accom_Type = accom_Type;
	}
	public String getFood_Desc() {
		return Food_Desc;
	}
	public void setFood_Desc(String food_Desc) {
		Food_Desc = food_Desc;
	}
	public String getBathroom_Type() {
		return Bathroom_Type;
	}
	public void setBathroom_Type(String bathroom_Type) {
		Bathroom_Type = bathroom_Type;
	}
	public String getPower_Backup() {
		return Power_Backup;
	}
	public void setPower_Backup(String power_Backup) {
		Power_Backup = power_Backup;
	}
	public String getOccupancy_Type() {
		return Occupancy_Type;
	}
	public void setOccupancy_Type(String occupancy_Type) {
		Occupancy_Type = occupancy_Type;
	}
	public String getRent_Amount() {
		return Rent_Amount;
	}
	public void setRent_Amount(String rent_Amount) {
		Rent_Amount = rent_Amount;
	}
	public String getWardrobe_Type() {
		return Wardrobe_Type;
	}
	public void setWardrobe_Type(String wardrobe_Type) {
		Wardrobe_Type = wardrobe_Type;
	}
	public String getTable() {
		return Table;
	}
	public void setTable(String table) {
		Table = table;
	}
	public String getChair() {
		return Chair;
	}
	public void setChair(String chair) {
		Chair = chair;
	}
	public String getGeyser() {
		return Geyser;
	}
	public void setGeyser(String geyser) {
		Geyser = geyser;
	}
	public String getTotal_Floor() {
		return Total_Floor;
	}
	public void setTotal_Floor(String total_Floor) {
		Total_Floor = total_Floor;
	}
	public String getRoomOn_Floor() {
		return RoomOn_Floor;
	}
	public void setRoomOn_Floor(String roomOn_Floor) {
		RoomOn_Floor = roomOn_Floor;
	}
	public String getFan() {
		return Fan;
	}
	public void setFan(String fan) {
		Fan = fan;
	}
	public String getBed() {
		return Bed;
	}
	public void setBed(String bed) {
		Bed = bed;
	}
	public String getCCTV() {
		return CCTV;
	}
	public void setCCTV(String cCTV) {
		CCTV = cCTV;
	}
	public String getWardeb() {
		return Wardeb;
	}
	public void setWardeb(String Wardeb) {
		Wardeb = Wardeb;
	}
	public String getRO_Filter() {
		return RO_Filter;
	}
	public void setRO_Filter(String rO_Filter) {
		RO_Filter = rO_Filter;
	}
	public String getImmed_Avail() {
		return Immed_Avail;
	}
	public void setImmed_Avail(String immed_Avail) {
		Immed_Avail = immed_Avail;
	}
	public String getFuture_Avail_Dt() {
		return Future_Avail_Dt;
	}
	public void setFuture_Avail_Dt(String future_Avail_Dt) {
		Future_Avail_Dt = future_Avail_Dt;
	}
	public String getProp_Desc() {
		return Prop_Desc;
	}
	public void setProp_Desc(String prop_Desc) {
		Prop_Desc = prop_Desc;
	}
}
