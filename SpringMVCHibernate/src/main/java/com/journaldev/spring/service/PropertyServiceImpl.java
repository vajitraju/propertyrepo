package com.journaldev.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.Entities.Address;
import com.journaldev.spring.Entities.RoomDetails;
import com.journaldev.spring.dao.PropertyDAO;

@Repository
public class PropertyServiceImpl implements PropertyService {
	
	@Autowired
	private PropertyDAO propertyDAO;

	@Override
	@Transactional
	public List<Address> listAddresses() {
		return propertyDAO.listAddresses();
	}

	@Override
	@Transactional
	public List<RoomDetails> listRomDetails() {
		return propertyDAO.listRomDetails();
	}

}
