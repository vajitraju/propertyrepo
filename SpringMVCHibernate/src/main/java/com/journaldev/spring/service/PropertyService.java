package com.journaldev.spring.service;

import java.util.List;

import com.journaldev.spring.Entities.Address;
import com.journaldev.spring.Entities.RoomDetails;

public interface PropertyService {

	public List<Address> listAddresses();
	public List<RoomDetails> listRomDetails();
	
}
