package com.journaldev.spring.dao;

import java.util.List;

import com.journaldev.spring.Entities.Address;
import com.journaldev.spring.Entities.RoomDetails;

public interface PropertyDAO {

	public List<Address> listAddresses();
	public List<RoomDetails> listRomDetails();
}
