package com.journaldev.spring.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.journaldev.spring.Entities.Address;
import com.journaldev.spring.Entities.RoomDetails;

@Service
public class PropertyDAOImpl implements PropertyDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
		
	@Override
	public List<Address> listAddresses() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Address.class);
		System.out.println(">>>>>>>>>>>>>>>> " + criteria.list().size());
		return criteria.list();
	}
	
	@Override
	public List<RoomDetails> listRomDetails() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(RoomDetails.class);
		System.out.println("<<<<<<<<<<<<, " + criteria.list().size());
		return criteria.list();
	}
}
