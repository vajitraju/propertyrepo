package com.journaldev.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.journaldev.spring.Entities.Address;
import com.journaldev.spring.Entities.RoomDetails;
import com.journaldev.spring.model.AddressModel;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.PropertyModel;
import com.journaldev.spring.model.RoomDetailsModel;
import com.journaldev.spring.service.PropertyService;

@RestController
public class PropertyController {
	
	@Autowired
	private PropertyService propertyService;
	
	@RequestMapping(value = "/property", method = RequestMethod.GET)
	@ResponseBody 
	public String listPersons(Model model) throws JsonProcessingException {
		List<Address> addressListEntity = propertyService.listAddresses();
		List<RoomDetails> roomListEntity = propertyService.listRomDetails();
		AddressModel addressModel = new AddressModel();
		RoomDetailsModel roomDetailsModel = new RoomDetailsModel();
		List<PropertyModel> propertyModelList = new ArrayList<PropertyModel>();
		for (Address address : addressListEntity) {
			BeanUtils.copyProperties(address, addressModel);
		}
		
		for (RoomDetails roomDetails : roomListEntity) {
			BeanUtils.copyProperties(roomDetails, roomDetailsModel);
		}
		
		PropertyModel propertyModel = new PropertyModel();
		propertyModel.setAddress(addressModel);
		propertyModel.setRoomDetails(roomDetailsModel);
		ObjectMapper mapper = new ObjectMapper();
		String propertyJson = mapper.writeValueAsString(propertyModelList);
		System.out.println(propertyJson);
		return propertyJson.toString();
	}
}
